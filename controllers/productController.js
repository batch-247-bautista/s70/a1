const Product = require("../models/Product");





module.exports.addProduct = (data) => {

	if(data.isAdmin){

		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});

		return newProduct.save().then((product, error) => {
			if(error){
				return false;
			} else {
				return {
					message: "New product successfully created!"
				}
			};
		});

	} 

	let message = Promise.resolve({
		message: "User must be an Admin to access this!"
	})

	return message.then((value) => {
		return value;
	})

};

module.exports.getAllProduct = () => {
	if (!user || !user.isAdmin) {
		throw new Error('Unauthorized');
	}
	return Product.find({}).then(result => {
		return result
	});
};

module.exports.getAllActive = () => {

	return Product.find({isActive : true}).then(result => {
		return result;
	});
};

module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result
	});
};

module.exports.updateProduct = (reqParams, reqBody) => {
	
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.id, updatedProduct).then((product, error) => {

		if(error){
			return false
		} else {
			let message = `Successfully updated product Id - "${reqParams.id}"`

			return message;
		}
	})
}

module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.id, updateActiveField).then((product, error) => {

		if(error){
			return false;
		} else {
			return true;
		};
	});
};


module.exports.activateProduct = (reqParams) => {
  let updateActiveField = {
    isActive : true
  };

  return Product.findByIdAndUpdate(reqParams.id, updateActiveField).then((product, error) => {
    if(error){
      return false;
    } else {
      return true;
    };
  });
};
